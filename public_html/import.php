<?php

set_time_limit(120);

define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ?: 'production'));
define('APP_PATH', __DIR__ . '/../app/');
define('PUBLIC_PATH', __DIR__);

include("../vendor/autoload.php");
$container = include(APP_PATH . "config/bootstrap.php");


$controller = new \Beoguma\Controller\ImportController($container);
$controller->import();
