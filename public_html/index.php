<?php

define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ?: 'production'));
define('APP_PATH', __DIR__ . '/../app/');
define('PUBLIC_PATH', __DIR__);

error_reporting(E_ALL);
ini_set('display_errors', 0);

if (!date_default_timezone_get()) {
    date_default_timezone_set("Europe/Belgrade");
}

include("../vendor/autoload.php");
$container = include(APP_PATH . "config/bootstrap.php");


$controller = new \Beoguma\Controller\WidgetController($container, $_SERVER['QUERY_STRING']);
$controller->renderWidget();
