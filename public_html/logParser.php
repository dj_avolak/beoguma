<?php
/**
 * Created by PhpStorm.
 * User: djavolak
 * Date: 11.9.16.
 * Time: 14.40
 */

$data = file_get_contents('../logs/missing.log');

$displayed = array();
foreach (explode(PHP_EOL, $data) as $row) {
    $key = trim(explode(':', $row)[5]);
    if (!in_array($key, $displayed)) {
        $displayed[] = $key;
        echo $key . PHP_EOL;
    }
}
