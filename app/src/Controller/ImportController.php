<?php
namespace Beoguma\Controller;

class ImportController
{
    /**
     * @var \Pimple\Container
     */
    protected $dic;

    public function __construct(\Pimple\Container $dic)
    {
        $this->dic = $dic;
    }

    public function import()
    {
        try {
            $feed = new \Beoguma\Model\FeedParser($this->dic);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}