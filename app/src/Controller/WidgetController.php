<?php
namespace Beoguma\Controller;

class WidgetController
{
    /**
     * @var \Pimple\Container
     */
    protected $dic;

    /**
     * @var \Beoguma\Model\Params
     */
    protected $params;

    public function __construct(\Pimple\Container $dic, $queryString)
    {
        $this->dic = $dic;
        $this->params = new \Beoguma\Model\Params($dic->offsetGet('db'), $queryString, $dic);
    }

    public function renderWidget()
    {
        //cache parsed data only, no need to cache db queries
        $itemProfile = $this->params->getItemProfile(false);
        $feed = new \Beoguma\Model\FeedSearch(
            $this->dic->offsetGet('cache')->get(\Beoguma\Model\FeedParser::FEED_CACHE_KEY)
        );

        $key = '\BEOGUMA\items-for-itemProfile-' . implode('-', $itemProfile);
        $data = unserialize($this->dic->offsetGet('cache')->get($key));
        if ($data === false) {
            $data = $feed->search($itemProfile);
            $this->dic->offsetGet('cache')->set($key, serialize($data), 60*30);
        }

        $letnje = $data['letnje'];
        $zimske = $data['zimske'];
        $model = $this->params->getModelWithVendor();

        //just render template
        include(APP_PATH . 'src/views/widget.phtml');
    }
}