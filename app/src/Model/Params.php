<?php

namespace Beoguma\Model;

/**
 * Class Params.
 * Represents mapping input params to internal mapping, in order to get appropriate tyre profile.
 *
 * @package Beoguma\Model
 */
class Params
{
    private $vendor;

    private $year;

    private $model;

    private $modification;

    /**
     * @var \Pimple\Container
     */
    private $dic;

    /**
     * @var \Pdo
     */
    private $db;

    public function __construct(\Pdo $db, $queryString, $dic)
    {
        $this->db = $db;
        $this->parseData($queryString);
        $this->dic = $dic;
    }

    public function getModelWithVendor()
    {
        return $this->vendor . ' ' . $this->model;
    }

    /**
     * Returns tyre profile for input params that are set. Consults mysql storage, and needs to be cached.
     *
     * @return array
     */
    public function getItemProfile($cache = true)
    {
        $key = '\BEOGUMA\item-profile-for-params-' . $this->model  .'-'. $this->vendor .'-'. $this->year .'-'. $this->modification;

        $profile = unserialize($this->dic->offsetGet('cache')->get($key));
        if ($profile === false) {
            $sql = "SELECT front_width, front_profile, front_diameter FROM `tx_tyrespecifications` WHERE carmodel = :modelId";
            $statement = $this->db->prepare($sql);
            $statement->execute([':modelId' => $this->getCarModel()]);
            $result = $statement->fetchObject();
            $profile = ['width' => $result->front_width, 'profile' => $result->front_profile, 'diameter' => $result->front_diameter];
            if ($cache) {
                $this->dic->offsetGet('cache')->set($key, serialize($profile), 60*30);
            }
        }

        return $profile;
    }

    /**
     * Parses query string into required data.
     *
     * @param $queryString
     *
     * @throws \Exception
     */
    protected function parseData($queryString)
    {
        $data = array();
        parse_str($queryString, $data);

        if (!isset($data['vendor'])) {
            throw new \Exception('no vendor param provided.');
        }
        if (!isset($data['year'])) {
            throw new \Exception('no year param provided.');
        }
        if (!isset($data['model'])) {
            throw new \Exception('no model param provided.');
        }
        if (!isset($data['modification'])) {
            #throw new \Exception('no modification param provided.');
        }

        //vendor mapping......................................................................................................
        if ($data['vendor'] == 'VW') {
            $data['vendor'] = 'Volkswagen';
        }

        //model mapping...
        $m = strtolower($data['model']);

        if ($m == 'astra') {
            $data['model'] = 'Astra G';
        } else if ($m == 'vectra') {
            $data['model'] = 'Vectra c';
        } else if ($m == 'golf 6') {
            $data['model'] = 'Golf VI';
        } else if ($m == 'golf 5') {
            $data['model'] = 'Golf V';
        } else if ($m == 'golf 4') {
            $data['model'] = 'Golf IV';
        } else if ($m == 'golf 3') {
            $data['model'] = 'Golf III';
        } else if ($m == 'golf 2') {
            $data['model'] = 'Golf II';
        }

        $this->model = $data['model'];
        $this->year = $data['year'];
        $this->vendor = $data['vendor'];
        $this->modification = $data['modification'];
    }

    /**
     * Consults storage with input params set, and returns the car model ID
     *
     * @throws \Exception
     *
     * @return string
     */
    protected function getCarModel()
    {
        //check mappings for model mismatch
        $model = \Beoguma\Model\Mapper::convert($this->model);
        if ($model) {
            $this->model = $model;
        }
        $sql = "SELECT id FROM tx_carmodels WHERE `model` = :model AND `vendor` = :vendor AND `year` = :year";
        $params = array(
            ':model' => $this->model,
            ':vendor' => $this->vendor,
            ':year' => $this->year
        );

        if ($this->modification !== '') {
            $sql .= " AND `modification` LIKE :modification";
            $params[':modification'] = '%' . $this->modification[0] . '%';
        }
        $statement = $this->db->prepare($sql);
        if (!$statement->execute($params)) {
            throw new \Exception('could not fetch tyre types.');
        }
        $rez = $statement->fetchAll();
        //not found with params as is
        if (!count($rez)) {
            //log missing params
            $this->logAndMailForMissingModel($params);

            //retry query, trying only with vendor this time
            $sql = "SELECT id FROM tx_carmodels WHERE `vendor` = :vendor LIMIT 1";
            $params = array(
                ':vendor' => $this->vendor,
            );
            $statement = $this->db->prepare($sql);
            if (!$statement->execute($params)) {
                throw new \Exception('could not fetch tyre types.');
            }
            $rez = $statement->fetchAll();
            //this should happen in extreme cases, log missing vendor and bail out for now
            if (!count($rez)) {
//                $this->logAndMailForMissingModel(array(':vendor' => $params[':vendor']));
                die();
            }
        }

        //disabled atm. always show first row
//        if ($statement->rowCount() > 1) {
//            throw new \Exception('found more than one car model.');
//        }

        return $rez[0]['id'];
    }

    /**
     * Logs missing models to file and sends mail. Should only log once per model.
     *
     * @param $params
     */
    protected function logAndMailForMissingModel($params)
    {
        $key = serialize($params);
        //new missing model, log it
        if ($this->dic->offsetGet('cache')->get($key) == false) {
            //save to redis
            $this->dic->offsetGet('cache')->set($key, 'true', -1);
            //log to file
            $msg = 'Missing model : ' . $params[':vendor'] .' '. $params[':model'] .' '.
                $params[':year'];
            $this->dic->offsetGet('missingModels')->info($msg);
            //mail
            $subject = 'Beoguma widget - no car model';
            $headers = 'From: apache@beoguma.mojauto.rs' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();
            //mail('djavolak@mail.ru', $subject, $msg, $headers);
        }
    }
}