<?php
namespace Beoguma\Model;

/**
 * Class FeedParser.
 * Class is responsible for fetching data from external source.
 *
 * @TODO all season variant group will go in other two groups. be careful about duplicates when searching.
 *
 * @package Beoguma\Model
 */
class FeedParser
{
    const FEED_CACHE_KEY = '\BEOGUMA\itemFeed';

    /**
     * @param \Pimple\Container $dic
     */
    public function __construct(\Pimple\Container $dic)
    {
        $this->parseFeed($dic);
    }

    /**
     * Clean up feed, parse it to collection and cache it
     */
    protected function parseFeed(\Pimple\Container $dic)
    {
        $sxe = $this->fetchFeed($dic);
        $data = new \ArrayObject();
        $i=0;
        $j=0;
        foreach ($sxe as $element) {
            $j++;
            if (
                //items not required
                ((string) $element->tip === '' || (string) $element->sezona === '')
            ) {
                continue;
            }
            //online items only for efficiency
            if ((string) $element->unavailable == 1) {
                continue;
            }
            if ((string) $element->online == 0) {
                continue;
            }
            $item = new \Beoguma\Model\Item();
            $item->setDescription(strip_tags(html_entity_decode((string) $element->description)));
            $item->setDiameter((string) $element->precnik);
            $item->setWidth((string) $element->sirina);
            $item->setProfile((string) $element->profil);
            $item->setManufacturer(trim((string) $element->manufacturer));
            $item->setName(htmlspecialchars(trim((string) $element->name), ENT_QUOTES, 'UTF-8'));
            $item->setPrice((string) $element->baseprice);
            $item->setStatus((bool) $element->unavailable && (bool) $element->online);
            $item->setSeason((string) $element->sezona);
            $item->setVariant((string) $element->tip);
            $item->setVendorcode((int) $element->vendorcode);
//            $item->setImage('http://portal.wings.rs/scripts/imagemanager.php?las_imgId='
//                . $element->vendorcode . '&las_imgSize=large&portal=75');
            $item->setImage($this->parseImageName($item->getName(), $item->getManufacturer(), $dic));
            $data->append($item);
            $i++;
        }
        //use redis storage
        $dic->offsetGet('cache')->set(self::FEED_CACHE_KEY, serialize($data));
        echo sprintf('Feed successful. Items in feed: %s. Feed now contains %s items', $j, $data->count()) . PHP_EOL;
    }

    protected function parseImageName($name, $manufacturer, $dic)
    {
        $words = array();
        $arr = explode(' ', trim(strtolower($name)));
        foreach ($arr as $key => $word) {
            if ($key != count($arr) - 1) {
                $words[] = str_replace('/', '', $word);
            }
        }
        $imagePath = PUBLIC_PATH . '/images/' . strtolower($manufacturer) . '.' . implode('.', $words) . '.jpg';
        if (is_file($imagePath)) {
            return '/images/' . strtolower($manufacturer) . '.' . implode('.', $words) . '.jpg';
        }
        $dic->offsetGet('log')->notice(sprintf('item %s is missing image.', $name));
        //echo sprintf('guma : %s ---> ochekivano ime fajla ---> %s', $name, '/images/' . strtolower($manufacturer) . '.' . implode('.', $words) . '.jpg') . PHP_EOL;

        return '';
    }


    /**
     * Fetch feed.
     *
     * @param \Pimple\Container $dic
     *
     * @return \SimpleXMLElement
     *
     * @throws \Exception
     */
    protected function fetchFeed(\Pimple\Container $dic)
    {
        try {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $dic->offsetGet('config')->feedPath);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_USERPWD, $dic->offsetGet('config')->feedUser . ":" . $dic->offsetGet('config')->feedPass);
            $res = curl_exec($curl);
            if (curl_errno($curl) !== 0) {
                var_dump(curl_errno($curl));
                var_dump(curl_error($curl));
                die('could not get feed.');
            }
            curl_close($curl);

            return new \SimpleXMLElement($res, LIBXML_NOCDATA);
        } catch (\Exception $e) { // unable to parse feed...
            throw $e;
        }
    }
} 