<?php

namespace Beoguma\Model;


class Mapper
{
    /**
     * Mapping between mojauto db and beoguma db params for car model value.
     * mojauto => beoguma; basically convert input to local storage value
     *
     * @var array
     */
    public static $models = array(
        'ForTwo' => 'Fourtwo'
    );

    /**
     * Convert input value to local value, if found. Return false otherwise.
     *
     * @param $value
     * @return bool|mixed
     */
    public static function convert($value)
    {
        foreach (self::$models as $input => $local) {
            if ($input === $value) {
                return $local;
            }
        }

        return false;
    }
}