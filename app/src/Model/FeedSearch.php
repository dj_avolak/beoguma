<?php

namespace Beoguma\Model;


class FeedSearch
{
    /**
     * @var \ArrayObject
     */
    private $storage;

    /**
     * cache params through the looping
     * @var
     */
    private $itemProfile;

    public function __construct($data)
    {
        $this->storage = unserialize($data);
    }

    public function getStats()
    {
        return 'Total items : ' . $this->storage->count();
    }

    /**
     * @TODO Separation of tyres is required in a sense that variant zimske and letnje are displayed separately.
     * config should decide which group is default to display
     *
     * @param $itemProfile
     *
     * @return array
     */
    public function search($itemProfile)
    {
        $this->itemProfile = $itemProfile;
        $zimske = array();
        $letnje = array();
        $result = array(
            'letnje', 'zimske'
        );

        /* @var \Beoguma\Model\Item $item */
        foreach($this->storage as $item) {
            if ($this->isValid($item)) {
                if ($item->getSeason() == 'letnje') {
                    $letnje[] = $item;
                } elseif ($item->getSeason() == 'zimske') {
                    $zimske[] = $item;
                } else {
                    $letnje[] = $item;
                    $zimske[] = $item;
                }
            }
        }

        //no appropriate tyre models found in storage
        if (empty($letnje) && empty($zimske)) {
            /* @var \Beoguma\Model\Item $item */
            foreach($this->storage as $item) {
                if ($this->isValidLoose($item)) {
                    if ($item->getSeason() == 'letnje') {
                        $letnje[] = $item;
                    } elseif ($item->getSeason() == 'zimske') {
                        $zimske[] = $item;
                    } else {
                        $letnje[] = $item;
                        $zimske[] = $item;
                    }
                }
            }
        }

        //Items implements toString method, which in turn prints out price. neat trick :)
        if (is_array($letnje)) {
            sort($letnje, SORT_STRING);
        }

        if (is_array($zimske)) {
            sort($zimske, SORT_STRING);
        }

        return array('zimske' => $zimske, 'letnje' => $letnje);
    }

    /**
     * Performs loose search, without diameter param, in order not to display empty box.
     *
     * @param Item $item
     * @return bool
     */
    protected function isValidLoose(\Beoguma\Model\Item $item)
    {
        if (
            $item->getProfile() == $this->itemProfile['profile']
            && $item->getDiameter() == 'R' . $this->itemProfile['diameter']
        ) {
            return true;
        }
        return false;
    }

    protected function isValid(\Beoguma\Model\Item $item)
    {
        if (
            $item->getWidth() == $this->itemProfile['width']
            && $item->getProfile() == $this->itemProfile['profile']
            && $item->getDiameter() == 'R' . $this->itemProfile['diameter']
        ) {
            return true;
        }
        return false;
    }
} 