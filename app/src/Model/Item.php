<?php

namespace Beoguma\Model;

class Item
{
    private $name;

    private $width;

    private $profile;

    private $diameter;

    private $description;

    private $price;

    private $manufacturer;

    private $image;

    private $season;

    private $variant;

    private $status;

    private $vendorcode;

    /**
     * @return mixed
     */
    public function getVendorcode()
    {
        return $this->vendorcode;
    }

    /**
     * @param mixed $vendorcode
     */
    public function setVendorcode($vendorcode)
    {
        $this->vendorcode = $vendorcode;
    }

    /**
     * @param mixed $season
     */
    public function setSeason($season)
    {
        $this->season = $season;
    }

    /**
     * @return mixed
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * @param mixed $variant
     */
    public function setVariant($variant)
    {
        $this->variant = $variant;
    }

    /**
     * @return mixed
     */
    public function getVariant()
    {
        return $this->variant;
    }



    /**
     * @param $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        if ($this->image == '') {
            return '/img/beoguma_placeholder.jpg';
        }

        return $this->image;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $diameter
     */
    public function setDiameter($diameter)
    {
        $this->diameter = $diameter;
    }

    /**
     * @return mixed
     */
    public function getDiameter()
    {
        return $this->diameter;
    }

    /**
     * @param mixed $manufacturer
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;
    }

    /**
     * @return mixed
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $profile
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
    }

    /**
     * @return mixed
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    public function compileUrl()
    {
        return 'http://www.beoguma.com/catalogsearch/result/?q=' . $this->vendorcode;

//        $url = 'http://www.beoguma.com/';
//        switch ($this->season) {
//            case "letnje":
//                $url .= 'letnje-gume-';
//                break;
//            case "zimske":
//                $url .= 'zimske-gume-';
//                break;
//            default;
//
//                break;
//        }
//        $url .= strtolower($this->manufacturer) .'-'. strtolower(str_replace(' ', '-', str_replace('+', '', $this->name)));
//
//        return $url;
    }

    public function compileParams()
    {
        return $this->width .'/'. $this->profile .' '. $this->diameter;
    }

    public function __toString()
    {
        return $this->price;
    }
}
