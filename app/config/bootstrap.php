<?php

$container = new \Pimple\Container();

$container['httpClient'] = function() {
    return new \Zend\Http\Client();
};

$container['missingModels'] = function() {
    $logDir = APP_PATH . '../logs/';
    $logFile = $logDir . '/missing.log';
    // create dir or file if needed
    if (!is_dir($logDir)) {
        umask(0002);
        mkdir($logDir);
    }
    if (!file_exists($logFile)) {
        umask(0002);
        touch($logFile);
    }

    $writer = new Zend\Log\Writer\Stream($logFile);
    $logger = new Zend\Log\Logger(array(
        'exceptionhandler' => false,
        'errorhandler' => false
    ));
    $logger->addWriter($writer);

    return $logger;
};

$container['log'] = function() {
    $date = new \DateTime('now', new \DateTimeZone('Europe/Belgrade'));
    $logDir = APP_PATH . '../logs/' . $date->format('Y') . '-' . $date->format('m');
    $logFile = $logDir . '/' . gethostname() . '-error.log';
    // create dir or file if needed
    if (!is_dir($logDir)) {
        umask(0002);
        mkdir($logDir);
    }
    if (!file_exists($logFile)) {
        umask(0002);
        touch($logFile);
    }

    $writer = new Zend\Log\Writer\Stream($logFile);
    $logger = new Zend\Log\Logger(array(
        'exceptionhandler' => true,
        'errorhandler' => true
    ));
    $logger->addWriter($writer);
//    $logger->addProcessor(new \Zend\Log\Processor\Backtrace());
//    $logger->registerFatalErrorShutdownFunction($logger);
//    $logger->registerExceptionHandler($logger);
//    $logger->registerErrorHandler($logger);

    return $logger;
};

/**
 * @TODO implement db pool
 */
$container['db'] = function($container) {
    $options = array(
        \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    );
    $dsn = 'mysql:host=' . $container->offsetGet('config')->database->host . ';dbname=' . $container->offsetGet('config')->database->dbname;

    return new \PDO($dsn, $container->offsetGet('config')->database->user,
        $container->offsetGet('config')->database->pass, $options);
};

$container['cache'] = function($container) {
    $cache = new \Redis();
    $cache->connect($container->offsetGet('config')->cache->host, $container->offsetGet('config')->cache->port);

    return $cache;
};

$container['config'] = function() {
    $config = include("config.php");

    return $config;
};

return $container;
