<?php

$params = array(
    'database' => array(
        'host'     => 'localhost',
        'user'     => 'root',
        'pass'     => 'somi00',
        'dbname'   => 'beoguma',
    ),
    'cache' => array(
        'host' => 'localhost'
    ),
    'feedPath' => 'ftp://ftp.beoguma.com/artikli.xml',
    'feedUser' => 'nonstopshop@beoguma.com',
    'feedPass' => 'ZmaT;uI(d?)D',
);

$localParams = include("config-local.php");

//merge configs overriding values with local params
foreach( $params as $key => $value ){
    if (in_array($key, array_keys($localParams))) {
        $params[$key] = $localParams[$key];
    }
}

$config = new \Zend\Config\Config($params);

return $config;